import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Container, Content, Header, Button} from 'native-base';
class single extends Component {
  constructor(props) {
    super(props);
    this.state = {
        capital:'',
        result:[],
        population:''
    };
  }
  
  componentWillMount(){
      let cname = this.props.navigation.getParam('capital');
      let population = this.props.navigation.getParam('population');
      this.setState({capital:cname,population:population});
  }
  render() {
    return (
        <Container>
            <Header>
                <Button
                    onPress={() => this.props.navigation.navigate('App')}
                >
                    <Text>Go Back</Text>
                </Button>
            </Header>
            <Content>
                <Text style={{padding:15}}>{this.state.capital}</Text>
                <Text style={{padding:15}}>{this.state.population}</Text>
            </Content>
        </Container>
    );
  }
}

export default single;
