import React from 'react';
import {TouchableOpacity} from 'react-native';
import {Container, Content, Header, Text, View} from 'native-base';
import single from './single';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
class App extends React.Component{
  constructor(){
    super();
    this.state = {country:[]};
  }
  getCountry(){
    fetch('https://restcountries.eu/rest/v2/all')
    .then((res) => res.json())
    .then((result) => {
      this.setState({country:result});
    })
  }
  componentWillMount(){
    this.getCountry();
  }
  render(){
    return(
      <Container>
        <Header></Header>
        <Content>
          {this.state.country.map((item) => {
            return(
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('single', {capital:item.capital, population:item.population})}
              >
              <Text style={{padding:15}}>{item.name}</Text>
              </TouchableOpacity>
            )
          })}
        </Content>
      </Container>
    )
  }
}
const navigation = createSwitchNavigator({
  App,
  single
})
export default createAppContainer(navigation);
